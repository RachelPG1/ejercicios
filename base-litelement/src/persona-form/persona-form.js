import { LitElement, html } from 'lit-element';

class PersonaForm extends LitElement {

    static get properties (){

        return  {
            person: {type: Object},
            editingPerson:{type:Boolean}
        };
    }

    constructor (){
        super();
        this.resetFormData();
       
    }


    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <div>
               <form>
                    <div class="form-group">
                        <label>Nombre completo</label>
                        <input id="personFormName" @input="${this.updateName}" .value="${this.person.name}" type="text" class="form-control" placeHolder="Nombre Completo" ?disabled="${this.editingPerson}"/>
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea @input="${this.updateProfile}" .value="${this.person.profile}" class="form-control" placeholder="Perfil" rows=5></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input type="text" @input="${this.updateYearsInCompany}" .value="${this.person.yearsInCompany}" class="form-control" placeHolder="Años en la empresa"/>
                    </div>
                    <button class="btn btn-primary" @click="${this.goBack}"><strong>Atrás</strong></button>
                    <button class="btn btn-success" @click="${this.storePerson}"><strong>Guardar</strong></button>

                    
                </form>
            </div>
        `;
    }

    resetFormData() {

        // Inicializar para cuando vengamos del botón + desde el principio y que no aparezca undefined
        console.log("resetFormData");

        this.person = {};
        this.person.name="";
        this.person.yearsInCompany="";
        this.person.profile="";
        this.editingPerson =false;
    }

    updateName (e) {
        console.log("updateName");
        console.log("Actualizando la propiedad name con el valor "+e.target.value);

        this.person.name= e.target.value;
    }

    updateProfile (e) {
        console.log("updateProfile");
        console.log("Actualizando la propiedad name con el valor "+e.target.value);
        this.person.profile= e.target.value;
    }

    updateYearsInCompany (e) {
        console.log("updateYearsInCompany");
        console.log("Actualizando la propiedad name con el valor "+e.target.value);
        this.person.yearsInCompany= e.target.value;
    }

    gotBack (e) {

        console.log("goBack");
        e.preventDefault();
        this.dispatchEvent(new CustomEvent("persona-form-close",{}));
        this.resetFormData();
    }

    storePerson(e) {
        console.log("storePerson");
        e.preventDefault();

        this.person.photo = {
            "src": "./img/persona.jpg",
            "alt": "Persona"
        };
    

        console.log ("La propiedad name vale "+this.person.name);
        console.log ("La propiedad profile vale "+this.person.profile);
        console.log("La propiedad yearsInCompany vale "+this.person.yearsInCompany);

        this.dispatchEvent (new CustomEvent ("persona-form-store",{
            detail: {
                person: {
                    name: this.person.name,
                    profile: this.person.profile,
                    yearsInCompany: this.person.yearsInCompany,
                    photo:this.person.photo
                    
                },
                editingPerson: this.editingPerson
            }
        }))
    }
}

customElements.define('persona-form', PersonaForm)