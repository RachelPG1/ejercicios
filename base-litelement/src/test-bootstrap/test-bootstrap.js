import { LitElement, html} from 'lit-element';

 

class TestBootstrap extends LitElement {

    render() {

        return html`

            <div>test bootstrap!</div>

        `;

    }

}

 

customElements.define('test-bootstrap', TestBootstrap)