import {LitElement, html} from 'lit-element';

class FichaPersona extends LitElement{

    static get properties () {
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type:String},
            photo: {type: Object}
        };
    }

    constructor () {
        super();

        this.name="Prueba Nombre";
        this.yearsInCompany="12";
        this.photo= {
            src:"./src/img/triki.jpg",
            alt:"Foto persona"
        }

        if (this.yearsInCompany >=7) {
            this.personInfo = "lead";
        }else if (this.yearsInCompany >=5){
            this.personInfo ="senior";

        }else if (this.yearsInCompany >=3){
            this.personInfo ="team";
        }else{
            this.personInfo ="others";
        }
    }

    updated (changedProperties) {
        changedProperties.forEach((oldValue, propName) => {
            console.log("Propiedad "+propName+" cambia valor, anterior era "+oldValue);
        }
        );

        if  (changedProperties.has("name")) {
            console.log ("Propiedad name cambia de valor anterior era "+ changedProperties.get(("name")+" nuevo es ")+ this.name);
        }
    }
    render(){

        return html`
        <div>
        <label for="fname">Nombre completo:</label>
        <input type="text" id="fname" name="fname" value="${this.name}" @change="${this.updateName}"}></input>
        <br/>
        <label for="yearsInCompany">Años en la empresa:</label>

        <input type="text" id="fage" name="yearsInCompany" value="${this.yearsInCompany}" @change="${this.updateYearsComponay}"></input>
        <br/>
        <label for="personInfo">${this.personInfo}</label>
        <img src="${this.photo.src}" height="200" width="200" alt="${this.photo.alt}">
        `;
    }

    updateName(e) {
        console.log ("updateName");
        this.name=e.target.value;
    }

    updateYearsComponay (e) {
        console.log ("updateYears");
        
        if (e.target.yearsInCompany >=7) {
            this.personInfo = "lead";
        }else if (this.yearsInCompany >=5){
            e.target.personInfo ="senior";

        }else if (this.yearsInCompany >=3){
            e.target.personInfo ="team";
        }else{
            e.target.personInfo ="others";
        }
    }
}

customElements.define('ficha-persona',FichaPersona);